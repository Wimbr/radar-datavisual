

class Pie  {
   
  float angle;
   float rotation;
  
  // internal variables
  QuartPie qA,qB,qC,qD ;
  
// constructor
   Pie(int _radius, float _angle, float _rotation)
   {
     angle = _angle ;
    rotation = _rotation ; 
     
     // pie instantiated
   //  color cA = color(122,0,0); 
    // color cD = color(122,0,255); 
     qA = new QuartPie(_radius,0,0);
     //qA.set_color(cA);
     qB = new QuartPie(_radius,0,-PI/2);
     qC = new QuartPie(_radius,0,PI);
     qD = new QuartPie(_radius,0,-1.5*PI);
    
      //qD.set_color(cD);
   }
   
 void set_angle(float a){
 this.angle= a;
 }
 
  void set_rotation(float r){
 this.rotation= r;
 }
 
 void set_radius(int r){
 
   qA.myRadius = r ;
   qB.myRadius = r ;
   qC.myRadius = r ;
   qD.myRadius = r ;
   
   
 }
   
 void set_color(color c){
qA.set_color(c);
qB.set_color(c);
qC.set_color(c);
qD.set_color(c);
   
 };
 
 void update(){
    
   if(angle<0){
    angle+=2*PI;
    
    }
    if(angle<=PI/2)
   {
        qA.set_angle(this.angle);
        qB.set_angle(0);
        qC.set_angle(0);
        qD.set_angle(0);
   }
   if((angle>PI/2)&&(angle<=PI))
   {
        qA.set_angle(PI/2);
        qB.set_angle(this.angle - PI/2);
        qC.set_angle(0);
        qD.set_angle(0);
   }
   if((angle>PI)&&(angle<=1.5*PI))
   {
        qA.set_angle(PI/2);
        qB.set_angle(PI/2);
        qC.set_angle(this.angle - PI);
        qD.set_angle(0);
   }
    if((angle>1.5*PI)&&(angle<=2*PI))
   {
        qA.set_angle(PI/2);
        qB.set_angle(PI/2);
        qC.set_angle(PI/2);
        qD.set_angle(this.angle - 1.5*PI);
   }
   
   
  pushMatrix(); 
rotate(rotation);

qA.update();
qB.update();
qC.update();
qD.update();
popMatrix();


   
}}
