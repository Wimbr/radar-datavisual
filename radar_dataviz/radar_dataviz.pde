// (c) https://ffunction.ca 2018 
// by Wim Bruyninckx
//
//
// #processing 3 
// 
// version ::  M1-c  a.k.a  the portrait colored  version
// 
// new in this version 
//
// - added optional color markers (0 and 1), 2 is nothing
// - changed the order of the countries so the empty space is at the
//   bottom right...
//
//
//
// the roundness is now calculated depending on the distance from the center

int dataIndex = 0 ;


String [][][] legend = 
{
        {
                {
                        "excited"
                } 
                , 
                {
                        "Q2-1", 
                        "Q4-1", 
                        "Q6-7",
                } 
                , 
                {
                        "legend1", 
                        "legend2", 
                        "legend3"
                }
        }
        , 
        {
                {
                        "knowledge"
                } 
                , 
                {
                        "Q5-3", 
                        "Q6-1", 
                        "Q9-9",
                } 
                , 
                {
                        "knowledge , legend1 ", 
                        "knowledge ,legend2", 
                        "knowledge ,legend3"
                }
        }
        , 
        {
                {
                        "negative"
                } 
                , 
                {
                        "Q6-3", 
                        "Q8-4", 
                        "Q9-8",
                } 
                , 
                {
                        "negative , legend1 ", 
                        "negative ,legend2", 
                        "negative ,legend3"
                }
        }
};



import processing.pdf.*;

Table question_a, question_b, question_c, question_d, question_e, question_f, question_g, question_h, question_i, question_j, question_k, question_l  ;
int EXCITED = 0 ; 
int KNOWLEDGE = 1 ;
int NEGATIVE = 2 ; 
int n_countries ; 
int COLS  = 3 ;
int ROWS = 5 ; 
int LIGHTCOLOR = 210 ; 
int MAP = 1 ; 
int NO_MAP = 0 ;
int MARKER_TYPE = 2 ; 
int MARKER_LINE_LENGTH= 8 ;
int MODE ; 
int DOT_SIZE = 8 ; 
int STROKE_WEIGHT = 5 ; 
int DIRECTORY = 1 ; 
int QUESTION = 2 ; 
color LIGHT_BLUE =  color(0, 200, 230)  ; 
color GREEN =  color(0, 180, 50)  ; 
color DARK_BLUE =  color(3, 78, 245)  ; 
color CYAN  =  color(0, 255, 255) ; 
color BLACK  =  color(0, 0, 0) ; 
color WHITE  =  color(255) ; 

float OFFSET_ANGLE = PI/12 ; 
float min_dom = -0.25;
float max_dom = 0.25 ; 
float ROTATION = PI/2 + 2 * PI/9 ;
boolean ONLY_SHAPES = false ; 
int ROUNDNESS = 28 ;  // deprecated
int minROUNDNESS = 20 ;
int maxROUNDNESS = 48 ; 
int STROKE_MIN = 1 ;
int STROKE_MAX = 5;
color colors[] = {
        CYAN, DARK_BLUE, GREEN, WHITE,
} 
;

// this is hardcoded 
int geoX[]  = { 
        388, 305, 1721, 1056, 1186, 1668, 2053, 220, 1496, 1322, 2006, 870, 872, 472
} 
;
int geoY[]  = { 
        901, 177, 823, 528, 242, 508, 403, 635, 238, 702, 694, 954, 272, 446
} 
;
//int order[] = { 
        //13, 0, 4, 7, 2, 11, 5, 12, 3, 10, 17,15, 1, 6
//}; // slot positions for the countries on the grid old map

// 
// the order depends on the ROW COL setting, for a ROW COL setting of 5 - 3 the slots are like this 

//     0  1  2 
//     3  4  5 
//     6  7  8
//     9  10 11
//     12 13 44


int order[] = { 
        9, 0, 10, 4, 2,7 , 8, 6, 5, 13, 11,12, 1, 3
};
boolean isEmerging[] = { 
        true, false, true, false, false, true, false, true, true, true, false, true, false, false
}; // slot positions for the countries on the grid

float[] values ; 
float[] avg, max, min , min_norm , max_norm  ;

int maxRadius ; 

PFont MMMFont, MMMFont_small, MMMFont_micro;

int source[] = {
        EXCITED, KNOWLEDGE, NEGATIVE
};





void setup() {

        // size((ONLY_SHAPES)?4400:2200, (ONLY_SHAPES)?2400:1200, P3D);
        background(255);
        pixelDensity(displayDensity());
        size(1200,1800);
        //surface.setResizable(true);
        //surface.setSize(1200,2400); 
        //size(4800,2400,P3D);
        String questionURL = legend[dataIndex][0][0];
        //String questionURL = "q6.3+6.7";
        //String questionURL = "q4b+q5.b+q6.2";
        beginRecord(PDF, "combo.pdf"); 
        MMMFont_small = createFont("PlusJakartaSans-Bold.ttf", 32);
        MMMFont_micro = createFont("PlusJakartaSans-Bold.ttf", 12);
        // background(220);

    

        noFill();  

        // Mode
        MODE = NO_MAP ; 

        // KNOWLEDGE VS NEGATIVE

        // loading the data 
        question_a = loadTable(legend[source[0]][0][0] +"/" + legend[source[0]][1][0]  + ".csv", "header" ) ;
        question_b = loadTable(legend[source[0]][0][0] +"/" + legend[source[0]][1][1]  + ".csv", "header" ) ;
        question_c = loadTable(legend[source[0]][0][0] +"/" + legend[source[0]][1][2]  + ".csv", "header" ) ;
        question_d = loadTable(legend[source[1]][0][0] +"/" + legend[source[1]][1][0]  + ".csv", "header" ) ;
        question_e = loadTable(legend[source[1]][0][0] +"/" + legend[source[1]][1][1]  + ".csv", "header" ) ;
        question_f = loadTable(legend[source[1]][0][0] +"/" + legend[source[1]][1][2]  + ".csv", "header" ) ;


        question_g = loadTable(legend[source[2]][0][0] +"/" + legend[source[2]][1][0]  + ".csv", "header" ) ;
        question_h = loadTable(legend[source[2]][0][0] +"/" + legend[source[2]][1][1]  + ".csv", "header" ) ;
        question_i = loadTable(legend[source[2]][0][0] +"/" + legend[source[2]][1][2]  + ".csv", "header" ) ;

     
        n_countries = question_a.getRowCount();


   

        if (MODE == NO_MAP) {
                //move to starting point
                translate(width/COLS/2, height/ROWS/2);
        } 


        
        maxRadius = (int) (0.55 * width/COLS) ; 

        Table[] questions = {
                question_a, question_b, question_c, question_d, question_e, question_f, question_g, question_h, question_i
        };
        avg = new float[questions.length];
        min = new float[questions.length];
        max = new float[questions.length];
        min_norm = new float[questions.length];
        max_norm = new float[questions.length];

        for ( int k = 0; k < questions.length; k++) {

                float sumRow = 0 ; 
                float [] aRow = new float[n_countries] ; 

                for ( int i = 0; i < n_countries; i++) {

                        // sum all i-th value
                        sumRow+= questions[k].getFloat(i, "answer" );
                        aRow[i] = questions[k].getFloat(i, "answer" );
                        //rowData[i][k] = questions[k].getFloat(i , "answer" );
                }

                avg[k] = sumRow/n_countries ; 
                min[k] = min(aRow);
                min_norm[k] = min[k]-avg[k];
                max[k] = max(aRow);
                max_norm[k] = max[k]-avg[k];
        }



      

        for ( int i = 0; i < n_countries; i++) {

                // restore future transformations
                pushMatrix();
                
                //
                
                
                // set axis to loca center

                if (MODE == NO_MAP) {
                        //the line below places the tiles in a brick style pattern
                        //translate((order[i]%COLS)*width/COLS + (((int)Math.floor(order[i]/8)==1)?width/COLS/2:0), floor(order[i]/COLS)* height/ROWS);
                        translate((order[i]%COLS)*width/COLS , floor(order[i]/COLS)* height/ROWS);
                } else { 
                        translate(geoX[i], geoY[i]);
                };

                rotate(-ROTATION);
 
                //for testing purposes only
                
                //stroke(0);
                //rect(-maxRadius,-maxRadius,maxRadius*2,maxRadius*2);


                // store question data in a local array for easy access

                values = new float[questions.length];
                for ( int k = 0; k < questions.length; k++) {
                        values[k] = questions[k].getFloat(i, "answer" )-avg[k];
                        // println(values[k]);
                };


                // or draw the shape

                fill((isEmerging[i])?color(0, 0, 255):color(255, 0, 0)); // we give this 2 colors that we afterwards change in vector drawing program into gradients
                noStroke();
                //stroke(0);

                if(ONLY_SHAPES||true){

                              noFill();

                stroke(0); 
                
                
                              // blobs smaller than average
                
                stroke(LIGHTCOLOR); 
                float[][] inter = interpolate(max_norm , values , 10);
                float cubic_level_norm , roundness ;
                for(int level = 0  ; level < inter.length ; level++ ) 
                {
                  cubic_level_norm = -sqrt(1 - pow((float)level/inter.length , 3 ))+1 ; 
                  roundness = map((float) level/inter.length , 0 , 1 , ROUNDNESS + 20 , ROUNDNESS);
               //strokeWeight(map(cubic_level_norm , 0 , 1 , STROKE_MIN  , STROKE_MAX));
                  strokeWeight(1);
                  blob(inter[level] ,  min_dom , max_dom ,  roundness  , maxRadius);
                }
                
                 // blobs bigger than average
                inter = interpolate( min_norm , values , 10);
                for(int level = 0  ; level < inter.length ; level++ ) 
                {
                  cubic_level_norm = -sqrt(1 - pow((float)level/inter.length , 3 ))+1 ; 
                  roundness = map((float) level/inter.length , 0 , 1 , ROUNDNESS - 20 , ROUNDNESS);
                  //strokeWeight(map(cubic_level_norm , 0 , 1 , STROKE_MIN , STROKE_MAX));
                  strokeWeight(1);
                  blob(inter[level] ,  min_dom , max_dom , roundness  , maxRadius);
                }
                
                // values - blob
                stroke(0);
                strokeWeight(STROKE_MAX);

                blob(values , min_dom , max_dom , ROUNDNESS  , maxRadius ) ;
                //stroke(GREEN);
                //blobSegment(values , min_dom , max_dom , ROUNDNESS  , maxRadius ,0 ,1 ) ;
                //stroke(CYAN);
                //blobSegment(values , min_dom , max_dom , ROUNDNESS  , maxRadius ,3 ,4 ) ;
                //stroke(DARK_BLUE);
                //blobSegment(values , min_dom , max_dom , ROUNDNESS  , maxRadius ,6 ,7 ) ;
               
                stroke(0);
                
                // labellocation
               //
                
               // rotate(ROTATION);

                textFont(MMMFont_micro);
                int labelOffset = 20; 
                for(int k = 0 ; k < values.length ; k++){
                
                 int k_ = (int) Math.floor(k/3); 
                
                  PVector  markerPosition   = new PVector ( map(values[k%values.length] , 
                min_dom  , max_dom  , 0 , maxRadius) , 0 ).rotate(2 *PI * (k%values.length)/values.length);

                 PVector  labelPosition   = new PVector ( map(values[k%values.length] , 
                min_dom  , max_dom  , 0 , maxRadius) + labelOffset , 0 ).rotate(2 *PI * (k%values.length)/values.length);

                
                      pushMatrix();
                 translate(labelPosition.x , labelPosition.y);
                 rotate(ROTATION);
                fill(0);
                 text(char(65 +k) , 0 ,0);

                  if(MARKER_TYPE == 1){
                 
                    stroke((k_==0)? GREEN : (k_==1) ? DARK_BLUE : CYAN );
                    strokeWeight(STROKE_MIN*2);
                    line(1-MARKER_LINE_LENGTH/2,4,1+MARKER_LINE_LENGTH/2 ,4);

                  }

                  popMatrix();
             
              if(MARKER_TYPE == 0 )
              {
                pushMatrix();
                noStroke();
                fill((k_==0)? GREEN : (k_==1) ? DARK_BLUE : CYAN );
                translate(markerPosition.x, markerPosition.y);
                ellipse(0,0,STROKE_MAX,STROKE_MAX);
                popMatrix();
              
              };

                  // rotate(-ROTATION);
                 // translate(-labelPosition.x , -labelPosition.y);

                }

               }            

                if (!ONLY_SHAPES) {


                        // draw the contour

                        textAlign(CENTER);
                        textFont(MMMFont_small);
                        for (int j = 0; j < values.length; j++) {

                                // draw the lines
                                noFill();
                                strokeWeight(2);
                                stroke(LIGHTCOLOR);                        };
                        // draw the dots

                        
                        // add the label
                        noStroke();
                        fill(0);
                        textAlign(CENTER);
                        rotate(ROTATION);
                        text( question_a.getString(i, "country" ), 00, 0 );
                }

                // reset the transformation matrix 
                popMatrix();
        };


        //drawLegend();


        //


        endRecord();
        if (ONLY_SHAPES)save("screenshot.png");
}


void drawQuestionMarks() {
        // add the question references

                fill(0);
        for (int j = 0; j < values.length; j++)
        {



                // fill((values[j]<=0)?(colors[(int)Math.floor(j/3)], map(values[j], 0, -0.1, 0, 255)):(colors[(int)Math.floor(j/3)], map(values[j], 0, 0.1, 0, 255)));
                text(j, getCoordinate(j%values.length, 0).x, getCoordinate(j%values.length, 0).y+DOT_SIZE/4 );
                //vertex();
        };
}


void drawLegend() {

        // add the legend
        textAlign(LEFT);
        int dot_offset = 20 ; 
        for (int j = 0; j < values.length; j++) {
                //     int ypos = (height-75 ) +  20*(j%3) ; 
                //   int xpos =  60+((int)Math.floor(j/3)* 600); 

                int ypos = 0 +  20*(j%3) ; 
                int xpos =  0 +((int)Math.floor(j/3)* 600); 

                fill(colors[j%colors.length]);
                strokeWeight(2);
                stroke(LIGHTCOLOR);
                ellipse(xpos-dot_offset, ypos-(dot_offset/2), DOT_SIZE, DOT_SIZE  );
                noStroke();
                fill(0);
                text(legend[source[(int)Math.floor(j/3)]][QUESTION][j%3], xpos, ypos );
                println(legend[source[(int)Math.floor(j/3)]][QUESTION][j%3]);
        }
}
;

PVector getCoordinate(int j, float offset) {
        return new PVector( map(values[j%values.length] + offset, min_dom, max_dom, 0, maxRadius )     * cos(2 * PI * j/values.length), 
        map(values[j%values.length] +offset, min_dom, max_dom, 0, maxRadius )    * sin( 2 * PI * j/values.length));

}



color setOpacity(color c , float amount){
  color color_  = color(red(c) , green(c) , blue(c) , map(amount , 0.0 , 1.0 , 0 , 255));
  return color_ ;


}

void blobSegment( float [] normData  , float minDomain , float maxDomain , float roundness ,  float radius , int index , int segmentLength ){

  float gamma = 2 * PI/6 ;
  float delta = (2 * PI - gamma*6)/3;

  beginShape();
    
  //fill(colors[0], map(values[j%values.length], 0, 0.1, 0, 255));
 // fill(0);
  for (int j = index; j <= segmentLength ; j++)
  {
          PVector  absolute      = new PVector ( map(normData[j%normData.length] , 
                minDomain , maxDomain , 
                0         , radius) , 0 ).
            rotate(2 *PI * (j%normData.length)/normData.length);
         
          PVector  absolute_next = new PVector ( map(normData[(j+1)%normData.length] , 
                minDomain , maxDomain , 
                0 , radius) , 0 ).
            rotate(2 * PI * ((j+1)%normData.length)/normData.length);
        // println(absolute); 
         
        // make the magnitude of the roundness dependend on the normalized data 
            
     
      //PVector ortho  = new PVector(1 , 0).setMag(roundness).rotate(HALF_PI + 2 *PI * (j%normData.length)/normData.length);
          //PVector ortho_ = new PVector(1 , 0).setMag(roundness).rotate(-HALF_PI + 2 *PI * ((j+1)%normData.length)/normData.length);
          PVector ortho  = new PVector(1 , 0).setMag(  map(normData[j%normData.length],
                minDomain , maxDomain ,  
                minROUNDNESS , maxROUNDNESS)).rotate(HALF_PI + 2 *PI * (j%normData.length)/normData.length);
          PVector ortho_ = new PVector(1 , 0).setMag(  map(normData[(j+1)%normData.length],
             minDomain , maxDomain , 
            minROUNDNESS , maxROUNDNESS)).rotate(-HALF_PI + 2 *PI * ((j+1)%normData.length)/normData.length);
    

          // we need to put a first point - so only when j == index

          if(j==index){

            vertex(absolute.x , absolute.y);
          }

          bezierVertex(
              absolute.x      + ortho.x  , 
              absolute.y      + ortho.y  , 
              absolute_next.x + ortho_.x ,   
              absolute_next.y + ortho_.y , 
              absolute_next.x                     , 
              absolute_next.y            
          );
  // need to rotate or not?
  };
endShape();




}




void blob( float [] normData  , float minDomain , float maxDomain , float roundness ,  float radius ){

  float gamma = 2 * PI/6 ;
  float delta = (2 * PI - gamma*6)/3;

  beginShape();
    
  //fill(colors[0], map(values[j%values.length], 0, 0.1, 0, 255));
 // fill(0);
  for (int j = 0; j <= normData.length; j++)
  {

          //// map the normalized from
          //
          // minDomain ---- 0 ----- maxDomain
          //
          // to:
          //
          // 0 ----------- 0.5 ------------ 1
          //
          //

   
        

          PVector  absolute      = new PVector ( map(normData[j%normData.length] , 
                minDomain , maxDomain , 
                0         , radius) , 0 ).
            rotate(2 *PI * (j%normData.length)/normData.length);
         
         // PVector absolute = getAbsolute(normData[j%normData.length] , minDomain , maxDomain , radius , (j%normData.length)/normData.length ) ;

          PVector  absolute_next = new PVector ( map(normData[(j+1)%normData.length] , 
                minDomain , maxDomain , 
                0 , radius) , 0 ).
            rotate(2 * PI * ((j+1)%normData.length)/normData.length);
        // println(absolute); 
         
        // make the magnitude of the roundness dependend on the normalized data 
            
     
      //PVector ortho  = new PVector(1 , 0).setMag(roundness).rotate(HALF_PI + 2 *PI * (j%normData.length)/normData.length);
          //PVector ortho_ = new PVector(1 , 0).setMag(roundness).rotate(-HALF_PI + 2 *PI * ((j+1)%normData.length)/normData.length);
          PVector ortho  = new PVector(1 , 0).setMag(  map(normData[j%normData.length],
                minDomain , maxDomain ,  
                minROUNDNESS , maxROUNDNESS)).rotate(HALF_PI + 2 *PI * (j%normData.length)/normData.length);
          PVector ortho_ = new PVector(1 , 0).setMag(  map(normData[(j+1)%normData.length],
             minDomain , maxDomain , 
            minROUNDNESS , maxROUNDNESS)).rotate(-HALF_PI + 2 *PI * ((j+1)%normData.length)/normData.length);
    

          // we need to put a first point - so only when j == 0

          if(j==0){

            vertex(absolute.x , absolute.y);
          }

          bezierVertex(
              absolute.x      + ortho.x  , 
              absolute.y      + ortho.y  , 
              absolute_next.x + ortho_.x ,   
              absolute_next.y + ortho_.y , 
              absolute_next.x                     , 
              absolute_next.y            
          );
  // need to rotate or not?
  };
endShape();




}

float[][]interpolate(float[] data_a , float[] data_b , int steps)
  {
        float [][] data_i = new float[steps] [data_a.length];

        for (int d = 0 ; d < data_a.length ; d++){
            
              float steplength = (data_b[d] - data_a[d])/steps; 
              //  println("s" + steplength);
                for(int s = 0 ; s < steps ; s++){
            
                  data_i[s][d] = data_a[d] + (s) * steplength;

            }
        }
  return data_i ;

}



PVector getAbsolute(float normFloat , float minDomain , float maxDomain , float radius , float normIndex){

  return new PVector ( map(normFloat , minDomain , maxDomain , 0  , radius) , 0 ).rotate(2 *PI * normIndex);
  


}
